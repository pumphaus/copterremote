#include <QCoreApplication>
#include <QCborValue>
#include <QDataStream>
#include <QGamepad>
#include <QGamepadManager>
#include <QTimer>
#include <QVariantMap>

#include <QTextStream>

#include <QNetworkDatagram>
#include <QUdpSocket>

#include <QtDebug>

QString number(double num, int prec = 2)
{
    QString s = QString::number(num, 'f', prec);
    return s.rightJustified(7);
}

static constexpr int port = 45450;
static constexpr int broadcastPort = 45451;

QHostAddress discoverCopterHost()
{
    qDebug() << "Waiting for host...";

    QUdpSocket socket;
    socket.bind(QHostAddress::AnyIPv4, broadcastPort);

    QEventLoop loop;

    QHostAddress address;

    QObject::connect(&socket, &QUdpSocket::readyRead, [&]() {
        QNetworkDatagram datagram;
        while (socket.hasPendingDatagrams()) {
            datagram = socket.receiveDatagram();
            address = QHostAddress(QString::fromUtf8(datagram.data()));
        }

        loop.quit();
    });

    loop.exec();

    qDebug() << "Found host at" << address;

    return address;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    const auto pads = QGamepadManager::instance()->connectedGamepads();
    for (auto pad : pads) {
        qDebug() << "Connected pad" << pad << QGamepadManager::instance()->gamepadName(pad);
    }
    if (pads.isEmpty()) {
        qFatal("No gamepad connected!");
        return 1;
    }

    const int padId = pads[0];

    qDebug() << "Using" << QGamepadManager::instance()->gamepadName(padId);

    QGamepad gamepad(padId);

    QHostAddress address = discoverCopterHost();

    QUdpSocket socket;

    QTimer timer;
    timer.setTimerType(Qt::PreciseTimer);
    timer.setInterval(20);

    QTextStream out(stdout);

    bool enabled = false;
    QObject::connect(&gamepad, &QGamepad::buttonAChanged, [&](bool pressed) {
        if (!pressed) {
            enabled = !enabled;
        }
    });

    QObject::connect(&timer, &QTimer::timeout, [&]() {
        double climbRate = (gamepad.buttonL2() - gamepad.buttonR2()) * 0.2;
        double yawRate = gamepad.axisRightX() * 90;
        if (qAbs(yawRate) < 5) {
            yawRate = 0;
        }

        QVariantMap map;
        map["t"] = QDateTime::currentMSecsSinceEpoch();
        map["ena"] = enabled;
        map["a"] = QVariantList { 0., 0., 0. };
        map["ar"] = QVariantList { 0., 0., yawRate };
        map["cr"] = climbRate;

        const auto data = QCborValue::fromVariant(map).toCbor();
        socket.writeDatagram(data, address, port);

        out << '\r'
            << "Climb rate: " << number(climbRate) << "\t"
            << "Yaw rate: " << number(yawRate) << "\t"
            << "Enabled: " << enabled << "\t"
            << flush;
    });

    timer.start();

    return a.exec();
}

